FROM maven:3-jdk-8-slim as builder

ARG VERSION=0.0.0-SNAPSHOT

COPY . .
RUN mvn versions:set -DnewVersion=$VERSION && \
    mvn install

FROM openjdk:8-jdk-slim
ENV PORT 8010
ENV CLASSPATH /opt/lib

# copy pom.xml and wildcards to avoid this command failing if there's no target/lib directory
COPY --from=builder pom.xml target/lib* /opt/lib/

# NOTE we assume there's only 1 jar in the target dir
# but at least this means we don't have to guess the name
# we could do with a better way to know the name - or to always create an app.jar or something
COPY --from=builder target/*.jar /opt/app.jar

WORKDIR /opt
CMD ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-jar", "app.jar"]

EXPOSE 8010
